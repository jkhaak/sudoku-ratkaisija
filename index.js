#!/usr/bin/env node

const _ = require("lodash");

const BOARD = `
040900000
710000605
000005003

009400080
001070400
000000360

000050010
000003804
520080000
`.replace(/\s/gm, "");

function initBlockPositions() {
    function positionsForBlock(init) {
        const arr = [];

        for (let i = init; i < init + 9 * 3; i += 9) {
            arr.push([i, i + 1, i + 2]);
        }

        return arr.flat();
    }

    return BLOCK_INIT.map(positionsForBlock);
}

/** Blokkien aloitus indeksit */
const BLOCK_INIT = [0, 3, 6, 27, 30, 33, 54, 57, 60];
/** Kaksiulotteinen lista, jonka sisimmässä listassa on kaikki kyseisen blokin indeksit. */
const BLOCK_POSITIONS = initBlockPositions();

/**
 * Tarkistaa onko täysi
 * @param {string} board
 */
function boardFull(board) {
    return board.indexOf("0") < 0;
}

/**
 * @param {string} board
 * @param {number} line
 */
function getLine(board, line) {
    const start = line * 9;
    return board.substring(start, start + 9);
}

/**
 * @param {string} board
 * @param {number} column
 */
function getColumn(board, column) {
    const arr = [];
    for (let i = column; i < column + (9 * 9) - 8; i += 9) {
        arr.push(board[i]);
    }

    return arr.join("");
}

/**
 * @param {string} board
 * @param {number} block
 */
function getBlock(board, block) {
    const arr = [];
    const init = BLOCK_INIT[block];
    for (let i = init; i <= init + (2 * 9); i += 9) {
        arr.push(board.substring(i, i + 3));
    }
    return arr.join("");
}

/**
 * Tarkistaa seuraavat asiat
 * 1. tarkistaa onko jokaisella rivillä yksi kutakin numeroa
 * 2. tarkistaa onko jokaisella sarakkeella yksi kutakin numeroa
 * 3. tarkistaa onko jokaisessa lohkossa yksi kutakin numeroa
 *
 * @param {string} board
 */
function boardComplete(board) {
    // jos lauta on vajaa, turha tarkastaa
    if (!boardFull(board)) {
        return false;
    }

    // tarkista onko jokaisella rivillä yksi kutakin numeroa
    for (let i = 0; i < 9; i++) {
        const line = new Set(getLine(board, i).replace(/0/gm, ""));
        if (line.size < 9) {
            return false;
        }

        const column = new Set(getColumn(board, i).replace(/0/gm, ""));
        if (column.size < 9) {
            return false;
        }

        const block = new Set(getBlock(board, i).replace(/0/gm, ""));
        if (block.size < 9) {
            return false;
        }
    }

    return true;
}

/**
 * Tarkistaa onko sijainti vapaa, eli siinä on nolla
 *
 * @param {string} board
 * @param {number} pos
 */
function posFree(board, pos) {
    return board.charAt(pos) === "0";
}

/**
 * Asettaa annettuun paikkaan numeron
 *
 * @param {string} board
 * @param {number} pos
 * @param {number} num
 */
function setPos(board, pos, num) {
    const init = board.substring(0, pos);
    const rest = board.substring(pos + 1);

    return `${init}${num}${rest}`;
}

/**
 * @param {number} pos
 */
function getLineNumber(pos) {
    return Math.floor(pos / 9);
}

/**
 * @param {number} pos
 */
function getColumnNumber(pos) {
    return pos % 9;
}

/**
 * @param {number} pos
 */
function getBlockNumber(pos) {
    for (let i = 0; i < 9; i++) {
        if (BLOCK_POSITIONS[i].includes(pos)) {
            return i;
        }
    }

    throw Error(`out of bounds.`);
}

/**
 * @param {string} board
 */
function niceBoard(board) {
    const lines = _.chunk(board, 9).map((line, index) => {
        const prettyLine = [0, 3, 6].map((pos) => line.slice(pos, pos + 3).join(""))
            .join(
                "|",
            );

        if (index === 2 || index === 5) {
            const divider = "\n---+---+---";
            return prettyLine.concat(divider);
        }
        return prettyLine;
    });

    return lines.join("\n");
}

/**
 * @param {string} board
 * @param {number} pos
 */
function isValidBoard(board, pos) {
    // console.log(`\nCheck board from ${pos}`);
    // console.log(niceBoard(board));
    const regions = [
        getLine(board, getLineNumber(pos)),
        getColumn(board, getColumnNumber(pos)),
        getBlock(board, getBlockNumber(pos)),
    ];
    const result = regions
        .map((region) => region.replace(/0/gm, ""))
        .map((region) => {
            const regionSet = new Set(region);
            return region.length === regionSet.size;
        })
        .reduce((acc, val) => acc && val, true);
    // console.log(`regions: ${regions}, result: ${result}`);

    return result;
}

/**
 * @param {string} board
 * @param {number} pos = 0
 */
function sudoku(board, pos = 0) {
    // console.log();
    // console.log(`Inspectin position ${pos}`);

    if (boardFull(board)) {
        return boardComplete(board) ? board : undefined;
    }

    if (!posFree(board, pos)) {
        // console.log(`Skipping`);
        return sudoku(board, pos + 1);
    }

    // console.log(niceBoard(board));

    for (let i = 1; i <= 9; i++) {
        let newBoard = setPos(board, pos, i);

        if (!isValidBoard(newBoard, pos)) {
            // console.log(`Tried ${pos} ${i}`);
            continue;
        }

        // console.log(`Trying move ${pos} ${i}`);
        newBoard = sudoku(newBoard, pos + 1);

        if (newBoard) {
            return newBoard;
        }
    }

    return undefined;
}

// sudoku(board);
console.log("Starting with board:");
console.log(niceBoard(BOARD));

console.log("\nCalculating...");
const result = sudoku(BOARD);

console.log("Result: ");
console.log(niceBoard(result));
